import pygame
from abccamera import AbcCamera
import os
import random
import time


#from http://www.pygame.org/pcr/transform_scale/, author Frank Raiser
def aspect_scale(img,bx,by):
    """ Scales 'img' to fit into box bx/by.
     This method will retain the original image's aspect ratio """
    ix,iy = img.get_size()
    if ix > iy:
        # fit to width
        scale_factor = bx/float(ix)
        sy = scale_factor * iy
        if sy > by:
            scale_factor = by/float(iy)
            sx = scale_factor * ix
            sy = by
        else:
            sx = bx
    else:
        # fit to height
        scale_factor = by/float(iy)
        sx = scale_factor * ix
        if sx > bx:
            scale_factor = bx/float(ix)
            sx = bx
            sy = scale_factor * iy
        else:
            sy = by

    return pygame.transform.scale(img, (int(sx),int(sy)))


class AbcGame():

    
    def init_gamedata(self):
        image_dirs = os.listdir("images")
        self.known_words = dict()
        for image_dir in image_dirs:
            self.known_words[image_dir]=[]
            image_dir_path = os.path.join("images",image_dir)
            for image_path in os.listdir(image_dir_path):
                (root,ext) = os.path.splitext(image_path)
                if ext.lower() == ".jpg":
                    img=pygame.image.load(os.path.join(image_dir_path,image_path))
                    
                    scaled_img = aspect_scale(img,self.screen_info.current_w,self.screen_info.current_h)
                    self.known_words[image_dir].append(scaled_img)
            #in case of dirs with no jpgs
            if len(self.known_words[image_dir]) == 0:
                del self.known_words[image_dir]

    def init_pygame(self):
        pygame.init()
        self.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        #self.screen = pygame.display.set_mode((800, 800),pygame.DOUBLEBUF)
        self.screen_info = pygame.display.Info()

        pygame.font.init()
        self.myfont = pygame.font.SysFont('Comic Sans MS', 50)
    
    def __init__(self):
        self.init_pygame()
        self.init_gamedata()
        

 
    def start_game_loop(self):
        done = False

        letters = ""
        old_letters = ""
        

        abccamera = AbcCamera()
        camthread = abccamera.start()
        time.sleep(3.0)
        update_frame = True
        set_ring = True
        increment = 1
        show_frame = False
        frame = None
        while not done:
                self.screen.fill((0,0,0))
                if abccamera.id_frame is not None and update_frame:
                    frame = pygame.surfarray.make_surface(abccamera.id_frame)
                #frame = pygame.surfarray.make_surface(abccamera.frame)
                if frame is not None and show_frame:
                    self.screen.blit(frame,(0,0))

                old_letters = letters

                letters = abccamera.word
 
                #print(letters)
                textsurface = self.myfont.render(letters.upper(), False, (255, 255, 255))
                self.screen.blit(textsurface,(self.screen_info.current_w/2,self.screen_info.current_h/2))
                if letters.strip("*") in self.known_words:
                    if letters != old_letters:
                        word_images = self.known_words[letters.strip("*")]
                        selected_image = random.choice(word_images)
                    self.screen.blit(selected_image,(0,0))
                else:
                    partial_matches = [x for x in self.known_words.keys() if x.startswith(letters.strip("*"))]
                    for match in partial_matches:
                        random.seed(sum([ord(x) for x in match]))
                        textsurface = self.myfont.render(match.upper(), False, (255, 255, 255))
                        self.screen.blit(textsurface,(self.screen_info.current_w*random.random(),self.screen_info.current_h*random.random()))

                for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                                done = True
                        if event.type == pygame.KEYDOWN:
                            
                            if event.key == pygame.K_q:
                                abccamera.stop()
                                camthread.join()
                                done = True
                            if event.key == pygame.K_f:
                                update_frame = not update_frame
                            if event.key == pygame.K_t:
                                show_frame = not show_frame
                            if event.unicode.isdigit():
                                if set_ring:
                                    selectedring = int(event.unicode)
                                    set_ring = not set_ring
                                else:
                                    selectedvalue = int(event.unicode)
                                    set_ring = not set_ring
                                    print("now setting ring %d, value %d"%(selectedring,selectedvalue))

                            if event.key == pygame.K_s:
                                abccamera.save_offsets()
                                print("saving offsets")
                            if event.key == pygame.K_z:
                                abccamera.change_offset(selectedring,selectedvalue,-increment)
                            if event.key == pygame.K_x:
                                abccamera.change_offset(selectedring,selectedvalue,increment)
                            if event.key == pygame.K_m:
                                increment += 1
                                print("increment %d"%increment)
                            if event.key == pygame.K_n:
                                increment -= 1
                                print("increment %d"%increment)
                            if event.key == pygame.K_BACKSPACE:
                                letters = letters[:-1]
                            else:
                                letters+=event.unicode                    
                
                pygame.display.flip()

if __name__ == "__main__":
    abcgame = AbcGame()
    abcgame.start_game_loop()
