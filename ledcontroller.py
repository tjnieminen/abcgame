import random
import time
import RPi.GPIO as GPIO
import threading

class LedController():
    def init_gpio(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        #self.ledpins = [23,17,27,22,19,16,20,26]
        #greens:
        self.greenpins = [23,17,19,16]
        #whites:
        self.whitepins = [27,22,20,26]
        self.ledgroups = [[23,17,20,26],[19,16,27,22]]
        for ledpin in self.greenpins+self.whitepins:
                GPIO.setup(ledpin,GPIO.OUT)
        self.greens_lit = False
    
    def __init__(self):
        self.init_gpio()
        self.stopped = False

    def start(self):
        # start the thread to read frames from the video stream
        thread = threading.Thread(target=self.update, args=())
        thread.start()
        return thread

    def stop(self):
        print("stop leds") 
        self.stopped = True
        
    def update(self):
        for ledpin in self.greenpins:
            GPIO.output(ledpin,GPIO.HIGH)
        starttime = time.time()
        active_leds = []
        while not self.stopped:
            """old_active_leds = active_leds
            random.shuffle(self.ledpins)
            active_leds = self.ledpins[0:4]
            for active_led in old_active_leds:
                if active_led not in active_leds:
                    GPIO.output(active_led,GPIO.LOW)
            for active_led in active_leds:
                if active_led not in old_active_leds:
                    GPIO.output(active_led,GPIO.HIGH)"""

            """if self.greens_lit:
                for ledpin in self.greenpins:
                    GPIO.output(ledpin,GPIO.LOW)
                for ledpin in self.whitepins:
                    GPIO.output(ledpin,GPIO.HIGH)
            else:
                for ledpin in self.whitepins:
                    GPIO.output(ledpin,GPIO.LOW)
                for ledpin in self.greenpins:
                    GPIO.output(ledpin,GPIO.HIGH)
            """
            time.sleep(0.25-((time.time()-starttime) % 0.25))
            #self.greens_lit = not self.greens_lit
        for led in self.whitepins + self.greenpins:
                GPIO.output(led,GPIO.LOW)
        return
