# README #

This is the repository for the software components of ABC Camera.

The code is in three Python scripts:
1. abccamera.py contains the image processing code that recognizes the letters blocks and return a string of letters for each combination of blocks.
2. gameloop.py runs the actual game, it displays the letters that have been recognized and the possible continuations for them. If a complete word has been formed with the blocks, gameloop.py displays an image corresponding to that word.
3. ledcontroller.py controls the lighting LEDs.

ring__offsets.txt is a settings file that defines the areas of an image where the letter markers should be checked for in the image processing stage.