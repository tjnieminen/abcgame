import pygame
import os
import random
import RPi.GPIO as GPIO
from picamera.array import PiRGBArray
from picamera import PiCamera
import threading
from ledcontroller import LedController
from collections import deque
import numpy as np
import time
import ast

import cv2

#This class is largely adapted from source code in http://www.pyimagesearch.com/2015/12/28/increasing-raspberry-pi-fps-with-python-and-opencv/ 
class AbcCamera():
    
    def init_camera(self):
        self.camera = PiCamera()
        time.sleep(2)

    def init_charmap(self):
        self.char_mappings = dict()
        raw_char_maps =\
        [\
                ("omlä","10001010"),\
                ("soia","00011001"),\
                ("ihmo","10001000"),\
                ("mnpi","01100000"),\
                ("keln","10000000"),\
                ("etur","01000000")\
        ]                      

        for (char_sides,mask) in raw_char_maps:
                for i in range(0,4):
                        self.char_mappings[mask[i*2:]+mask[:i*2]]=char_sides[i]


    def is_roi_occupied(self,image,inner_roi_corners,outer_roi_corners,counter,is_horizontal,tolerance=5):
        (rows,cols,channels) = image.shape
        center = (rows/2,cols/2)
        is_occupied = False
        for difference in range(-tolerance,tolerance+1):
            if is_horizontal:
                inner_roi = image[min(inner_roi_corners[0][1],inner_roi_corners[1][1])+difference:max(inner_roi_corners[0][1],inner_roi_corners[1][1])+difference,min(inner_roi_corners[0][0],inner_roi_corners[1][0]):max(inner_roi_corners[0][0],inner_roi_corners[1][0])]
                outer_roi = image[min(outer_roi_corners[0][1],outer_roi_corners[1][1])+difference:max(outer_roi_corners[0][1],outer_roi_corners[1][1])+difference,min(outer_roi_corners[0][0],outer_roi_corners[1][0]):max(outer_roi_corners[0][0],outer_roi_corners[1][0])]
            else:
                inner_roi = image[min(inner_roi_corners[0][1],inner_roi_corners[1][1]):max(inner_roi_corners[0][1],inner_roi_corners[1][1]),min(inner_roi_corners[0][0],inner_roi_corners[1][0])+difference:max(inner_roi_corners[0][0],inner_roi_corners[1][0])+difference]
                outer_roi = image[min(outer_roi_corners[0][1],outer_roi_corners[1][1]):max(outer_roi_corners[0][1],outer_roi_corners[1][1]),min(outer_roi_corners[0][0],outer_roi_corners[1][0])+difference:max(outer_roi_corners[0][0],outer_roi_corners[1][0])+difference]

            inner_hist = cv2.calcHist([inner_roi], [0, 1, 2], None, [8, 8, 8],[0, 256, 0, 256, 0, 256])
            #inner_hist = cv2.calcHist([inner_roi], [1], None, [8],[0, 256])
            
            outer_hist = cv2.calcHist([outer_roi], [0, 1, 2], None, [8, 8, 8],[0, 256, 0, 256, 0, 256])
            #outer_hist = cv2.calcHist([outer_roi], [1], None, [8],[0, 256])

            hist_comp = cv2.compareHist(inner_hist,outer_hist,cv2.HISTCMP_CORREL)
            #if there's border, check that the outer roi is brighter green.
            
            if hist_comp < 0.5:
                #return True
                inner_b,inner_g,inner_r = cv2.split(inner_roi)
                outer_b,outer_g,outer_r = cv2.split(outer_roi)
                #brightness requirement
                if outer_g.mean() < 60-10*counter:
                    continue
                #uniformity requirement, needs to be implemented differently, this one misclassifies because the partition isn't granular enough
                """(outer_rows,outer_cols,outer_channels) = outer_roi.shape
                if is_horizontal:
                    outer_part1 = outer_roi[0:outer_rows,0:int(outer_cols/2)]
                    outer_part2 = outer_roi[0:outer_rows,int(outer_cols/2):outer_cols]
                else:
                    outer_part1 = outer_roi[0:int(outer_rows/2),0:outer_cols]
                    outer_part2 = outer_roi[int(outer_rows/2):outer_rows,0:outer_cols]
                outer_hist1 = cv2.calcHist([outer_part1], [0, 1, 2], None, [8, 8, 8],[0, 256, 0, 256, 0, 256])
                outer_hist2 = cv2.calcHist([outer_part2], [0, 1, 2], None, [8, 8, 8],[0, 256, 0, 256, 0, 256])
                outer_hist_comp = cv2.compareHist(outer_hist1,outer_hist2,cv2.HISTCMP_CORREL)
                if outer_hist_comp < 0.1:
                    continue"""
                    
                if inner_g.mean()<outer_g.mean():
                    #cv2.putText(image,str(hist_comp),inner_roi_corners[0],cv2.FONT_HERSHEY_SIMPLEX,0.5,255)
                    return True
                    #check that there' s a green area outside the outer area
                    #if is_horizontal:
                    #    validation_roi_corners = outer_roi
    

        #cv2.putText(image,str(hist_comp),inner_roi_corners[0],cv2.FONT_HERSHEY_SIMPLEX,0.5,255)
        return is_occupied
        
        
        """roi_mean = roi.mean()
        adjusted_limit = int(70-counter*10)
        mask = cv2.inRange(roi,np.array([0,adjusted_limit,0]),np.array([adjusted_limit,255,adjusted_limit]))

        white_mask = cv2.inRange(roi,np.array([150,200,150]),np.array([255,255,255]))
        mask = cv2.bitwise_or(mask,white_mask)

        res = cv2.bitwise_and(roi,roi,mask=mask)
        gray_roi = cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY)

        nonzerocount = cv2.countNonZero(gray_roi)
        if nonzerocount > 0:
            proportion_of_green = cv2.countNonZero(mask)/cv2.countNonZero(gray_roi)
        else:
            proportion_of_green = 0
        return res.mean()/roi_mean > 0.3 and proportion_of_green > 0.8"""

    def crop_frame(self):
        (rows,cols,channels) = self.frame.shape
        M = cv2.getRotationMatrix2D((cols/2,rows/2),1.6,1)
        rotated_frame = cv2.warpAffine(self.frame,M,(cols,rows))
        #rotated_frame = self.frame
        #rotated_frame = dst.copy()
        (x_correction,y_correction) = (-10,0)
        self.cropped_frame = rotated_frame[rows/2-(self.id_size-x_correction):rows/2+(self.id_size+x_correction),cols/2-(self.id_size-y_correction):cols/2+(self.id_size+y_correction)]
        #self.cropped_frame = rotated_frame


    def save_offsets(self):
        with open("ring_offsets.txt",'w') as offset_file:
            for line in self.ring_offset_values:
                offset_file.write(str(line)+"\n")
     
    def change_offset(self,ring_index,offset_value_index,change):
        #print(ring_index)
        #print(offset_value_index)
        self.ring_offset_values[ring_index][offset_value_index] += change
        
    def identify_frame(self):
        #self.id_frame = self.cropped_frame
        (rows,cols,channels) = self.cropped_frame.shape
        center = (rows/2,cols/2)
        word = ""

        for [offset,x_trim,y_trim,roi_width,roi_length] in self.ring_offset_values:
                topleft = (offset+x_trim,offset+y_trim)
                bottomright = (self.id_size*2-(offset-x_trim),self.id_size*2-(offset-y_trim))
                
                maskstring = ""
                counter=0
                reverse_rois = True
                for (corner_x,corner_y) in [topleft,(bottomright[0],topleft[1]),bottomright,(topleft[0],bottomright[1])]:
                        (y_closer_x,y_closer_y) = (corner_x-roi_width,corner_y-roi_width if corner_y>self.id_size else corner_y+roi_width)
                        (y_further_x,y_further_y) = (corner_x+roi_width,corner_y-roi_length if corner_y>self.id_size else corner_y+roi_length)
                        
                        (x_closer_x,x_closer_y) = (corner_x-roi_width if corner_x>self.id_size else corner_x+roi_width,corner_y-roi_width)
                        (x_further_x,x_further_y) = (corner_x-roi_length if corner_x>self.id_size else corner_x+roi_length,corner_y+roi_width)

                        """(y_closer_x,y_closer_y) = (corner_x-roi_width,corner_y-roi_width*2 if corner_y>self.id_size else corner_y+roi_width*2)
                        (y_further_x,y_further_y) = (corner_x+roi_width,corner_y-roi_length if corner_y>self.id_size else corner_y+roi_length)
                        
                        (x_closer_x,x_closer_y) = (corner_x-roi_width*2 if corner_x>self.id_size else corner_x+roi_width*2,corner_y-roi_width)
                        (x_further_x,x_further_y) = (corner_x-roi_length if corner_x>self.id_size else corner_x+roi_length,corner_y+roi_width)"""

                        rois = [(x_closer_x,x_closer_y,x_further_x,x_further_y),(y_closer_x,y_closer_y,y_further_x,y_further_y)]
                        #reverse every other roi list to get clockwise order
                        if reverse_rois:
                                rois.reverse()
                                reverse_rois = False
                        else:
                                reverse_rois = True

                        for (roi_x1,roi_y1,roi_x2,roi_y2) in rois:
                                #(mean,proportion_of_green) = self.mean_green(self.cropped_frame,roi_x1,roi_y1,roi_x2,roi_y2,len(word))
                                
                                #print("%d: %f"%(counter,mean))

                                #if mean > 0.3 and proportion_of_green > 0.8:
                                horizontal_roi = abs(roi_x2-roi_x1)>abs(roi_y2-roi_y1)

                                if horizontal_roi:
                                    sub_roi1 = ((roi_x1,corner_y),(roi_x2,roi_y1))
                                    sub_roi2 = ((roi_x1,corner_y),(roi_x2,roi_y2))
                                    if (abs(center[0]-sub_roi1[1][1]) > abs(center[0]-sub_roi2[1][1])):
                                        inner_roi = sub_roi2
                                        outer_roi = sub_roi1
                                    else:
                                        inner_roi = sub_roi1
                                        outer_roi = sub_roi2
                                else:
                                    sub_roi1 = ((corner_x,roi_y1),(roi_x1,roi_y2))
                                    sub_roi2 = ((corner_x,roi_y1),(roi_x2,roi_y2))
                                    if (abs(center[1]-sub_roi1[1][0]) > abs(center[1]-sub_roi2[1][0])):
                                        inner_roi = sub_roi2
                                        outer_roi = sub_roi1
                                    else:
                                        inner_roi = sub_roi1
                                        outer_roi = sub_roi2

                                hist_comp = self.is_roi_occupied(self.cropped_frame,inner_roi,outer_roi,len(word),horizontal_roi)

                                if hist_comp:
                                        roi_color = (255,0,0)
                                        maskstring += "1"
                                else:
                                        roi_color = (0,0,255)
                                        maskstring += "0"
                                        
                                
                                counter+=1

                                outer_color=(255,255,255)
                                cv2.rectangle(self.cropped_frame,inner_roi[0],inner_roi[1],roi_color)
                                cv2.rectangle(self.cropped_frame,outer_roi[0],outer_roi[1],outer_color)
                                    
                                    
                        #if (roi.mean()[1]>150):
                        #        cv2.rectangle(combinedimage,x_rect_closer,x_rect_further,(255,0,255))
                #print(maskstring)
                #print(self.char_mappings)
                
                if maskstring not in self.char_mappings:
                        word+='*'
                else:
                        word+=self.char_mappings[maskstring]
        self.id_frame = self.cropped_frame
        self.word = word
        


    """def take_id_shot(self):
        self.camera.ISO = 50
        self.camera.shutter_speed = 30000
        self.camera.sharpness = 100
        self.camera.contrast = 0
        
        #camera.awb_mode = "flash"
        #camera.exposure_mode = "night"
        resolution = (2592, 1944)
        self.camera.resolution = resolution
        #camera.framerate = 32
        rawCapture = PiRGBArray(self.camera)

        #take a shot for each led group
        images = []
        for ledgroup in self.ledgroups:
                for led in ledgroup:
                        #print("setting LED %d on"%led)
                        GPIO.output(led,GPIO.HIGH)
                        #print("LED %d on"%led)
                self.camera.capture(rawCapture, format="bgr")

                for led in ledgroup:
                        GPIO.output(led,GPIO.LOW)
                img = rawCapture.array
                
                (rows,cols,channels) = img.shape
                roi = img[rows/2-695:rows/2+505,cols/2-570:cols/2+630]
                images.append(roi)

                # clear the stream in preparation for the next frame
                rawCapture.truncate(0)

        #combine ledgroup shots
        combinedimage = images[0]

        imagecount = 1
        for image in images[1:]:
                
                #cv2.imwrite("newtest%d.png"%imagecount,image)
                img2gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
                ret, mask = cv2.threshold(img2gray, 30, 255, cv2.THRESH_BINARY)
                mask_inv = cv2.bitwise_not(mask)
                res = cv2.bitwise_and(image,image,mask=mask)
                #combinedimage = cv2.add(combinedimage,res)
                #cv2.imshow("combined  image", combinedimage)
                #cv2.waitKey(0)
                #1.5 so that the colors get intensified
                weightdiff = 1.25/imagecount
                combinedimage = cv2.addWeighted(combinedimage,1.25-weightdiff,image,weightdiff,0)
                combinedimage = cv2.addWeighted(combinedimage,1,image,1,0)
                imagecount+=1

        #fix rotation error
        (rows,cols,channels) = combinedimage.shape
        M = cv2.getRotationMatrix2D((cols/2,rows/2),1.5,1)
        dst = cv2.warpAffine(combinedimage,M,(cols,rows))
        combinedimage = dst.copy()
        #cv2.imwrite("combinedimage.png",combinedimage)

        def mean_green(image,roi_x1,roi_y1,roi_x2,roi_y2,counter):
                roi = image[min(roi_y1,roi_y2):max(roi_y1,roi_y2),min(roi_x1,roi_x2):max(roi_x1,roi_x2)]
                
                roi_mean = roi.mean()
                adjusted_limit = int(70-counter*8)
                mask = cv2.inRange(roi,np.array([0,adjusted_limit,0]),np.array([adjusted_limit,255,adjusted_limit]))
                res = cv2.bitwise_and(roi,roi,mask=mask)
                gray_roi = cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY)
                proportion_of_green = cv2.countNonZero(mask)/cv2.countNonZero(gray_roi)
                print(proportion_of_green)
                return (res.mean()/roi_mean,proportion_of_green)

        #estimate ambient brightness by averaging over a square int the middle
        #ambient_roi = combinedimage[500:700,500:700]
        #ambient_brightness = ambient_roi.mean()
        #camera.shutter_speed = int(5000+10000*(1-255/ambient_brightness))
        #assign significant rectangles
        word = ""
        for (offset,x_trim,y_trim,roi_width,roi_length) in [(115,5,15,25,160),(290,5,0,18,100),(370,0,-5,10,150),(420,-5,0,6,50),(451,-10,-5,5,50)]:
                topleft = (offset+x_trim,offset+y_trim)
                bottomright = (1200-(offset-x_trim),1200-(offset-y_trim))
                #cv2.rectangle(combinedimage,topleft,bottomright,(255,0,0))
                maskstring = ""
                counter=0
                reverse_rois = True
                for (corner_x,corner_y) in [topleft,(bottomright[0],topleft[1]),bottomright,(topleft[0],bottomright[1])]:
                        (y_closer_x,y_closer_y) = (corner_x-roi_width,corner_y-roi_width*2 if corner_y>600 else corner_y+roi_width*2)
                        (y_further_x,y_further_y) = (corner_x+roi_width,corner_y-roi_length if corner_y>600 else corner_y+roi_length)
                        
                        (x_closer_x,x_closer_y) = (corner_x-roi_width*2 if corner_x>600 else corner_x+roi_width*2,corner_y-roi_width)
                        (x_further_x,x_further_y) = (corner_x-roi_length if corner_x>600 else corner_x+roi_length,corner_y+roi_width)

                        rois = [(x_closer_x,x_closer_y,x_further_x,x_further_y),(y_closer_x,y_closer_y,y_further_x,y_further_y)]
                        #reverse every other roi list to get clockwise order
                        if reverse_rois:
                                rois.reverse()
                                reverse_rois = False
                        else:
                                reverse_rois = True

                        for (roi_x1,roi_y1,roi_x2,roi_y2) in rois:
                                (mean,proportion_of_green) = mean_green(combinedimage,roi_x1,roi_y1,roi_x2,roi_y2,len(word))
                                
                                print("%d: %f"%(counter,mean))

                                if mean > 0.3 and proportion_of_green > 0.8:
                                        roi_color = (255,0,0)
                                        maskstring += "1"
                                else:
                                        roi_color = (0,0,255)
                                        maskstring += "0"
                                cv2.putText(combinedimage,str(counter),(roi_x1,roi_y1),cv2.FONT_HERSHEY_SIMPLEX,1,255)
                                counter+=1
                                cv2.rectangle(combinedimage,(roi_x1,roi_y1),(roi_x2,roi_y2),roi_color)
                        #if (roi.mean()[1]>150):
                        #        cv2.rectangle(combinedimage,x_rect_closer,x_rect_further,(255,0,255))
                #print(maskstring)
                #print(self.char_mappings)
                if maskstring not in self.char_mappings:
                        break
                else:
                        word+=self.char_mappings[maskstring]
        self.id_frame = combinedimage
        self.word = word
        
    def take_polling_shot(self):
        resolution = (640, 480)
        self.camera.resolution = resolution
        #camera.framerate = 32
        rawCapture = PiRGBArray(self.camera)
        self.camera.capture(rawCapture, format="bgr")
        img = rawCapture.array
        hist = cv2.calcHist([img], [0, 1, 2], None, [8, 8, 8],[0, 256, 0, 256, 0, 256])
        hist = cv2.normalize(hist,hist).flatten()
        return hist"""

    def stop(self):
        print("stopping cam thread")
        self.stopped = True
            
    def __init__(self):
        #self.init_gpio()
        self.init_camera()
        self.init_charmap()
        self.word = ""
        self.stopped = False

        self.stream = None
        self.rawCapture = None
        self.apply_poll_settings()
        
        self.ref_hist = None
        self.hist_comps = []
        self.hist_comp_variance = None
        self.change_detected = False

        self.id_frame = None

        self.id_size = 400

        #it might be good to introduce some stickiness to the ring bit values, but the frame rate might be too low for that, would be bad for responsiveness
        """self.ring_memory = []
        for i in range(0,5):
            self.ring_memory[i]=deque([0,0,0,0,0])"""
        
        with open("ring_offsets.txt") as ring_offset_file:
            self.ring_offset_values = [ast.literal_eval(line) for line in ring_offset_file]
        

    def start(self):
        # start the thread to read frames from the video stream
        thread = threading.Thread(target=self.update, args=())
        thread.start()
        return thread

    def apply_poll_settings(self):
        self.camera.ISO = 50
        #self.camera.exposure_mode = "fireworks"
        self.camera.color_effects = None
        self.camera.awb_mode = "off"
        self.camera.awb_gains = (0.7,0.7)
            #self.camera.shutter_speed = 10000
        self.camera.sharpness = 100
        resolution = (1640, 922)
        self.camera.resolution = resolution
        self.camera.framerate = 40
        self.rawCapture = PiRGBArray(self.camera,size=resolution)
        self.stream = self.camera.capture_continuous(self.rawCapture,format="bgr", use_video_port=True)

    #make this keep looping over the continuous frames, comparing histograms until there's a clear shift, then run id shot and update letters
    #all the game loop needs to know about this is what the current letters are!
    def update(self):
        ledcontroller = LedController()
        ledthread = ledcontroller.start()
        frame_counter= 0
        self.apply_poll_settings()
        last_time = time.time()
        for f in self.stream:
                if self.stopped:
                    break
                # grab the frame from the stream and clear the stream in
                # preparation for the next frame
                
                self.frame = f.array
                self.rawCapture.truncate(0)
                self.crop_frame()
                self.identify_frame()
                
                frame_counter+=1
                if (frame_counter % 100)==0:
                    print(time.time()-last_time)
                    last_time = time.time()
                    print(frame_counter)
                """new_hist = cv2.calcHist([self.frame], [0, 1, 2], None, [8, 8, 8],[0, 256, 0, 256, 0, 256])
                if self.ref_hist is not None:
                    new_hist_comp = cv2.compareHist(new_hist,self.ref_hist,cv2.HISTCMP_CORREL)
                    self.hist_comps.append(new_hist_comp)
                else:
                    self.ref_hist = new_hist

                if len(self.hist_comps)>10:
                    #check if the histogram comparisons show fluctuation
                    self.hist_comp_variance = np.var(self.hist_comps)
                    self.hist_comps = []
                    self.ref_hist = None
                    if (self.hist_comp_variance > 0.01):
                        self.change_detected = True
                    elif self.change_detected:
                        self.changed_detected = False
                        self.take_id_shot()
                        break"""
        print("calling led controller stop method")
        # if the thread indicator variable is set, stop the thread
        # and resource camera resources
        ledcontroller.stop()
        ledthread.join()
        print("cleaning cam thread")
        self.stream.close()
        self.rawCapture.close()
        self.camera.close()
        return

if __name__ == "__main__":
    abccamera = AbcCamera()
    abccamera.update()
